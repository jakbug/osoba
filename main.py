from helper import Human
from helper import Book
if __name__=='__main__':
    books=[
        Book('13 pięter', 'Filip Springer'),
        Book('Angole', 'Ewa Winnicka'),
        Book('Astrid Lindgren.Opowieść o życiu i twórczości', 'Margareta Strömstedt'),
        Book('Awantury na tle powszechnego ciążenia', 'Tomasz Lem'),
        Book('Bajka o Rašce i inne reportaże sportowe', 'Ota Pavel'),
        Book('Chłopczyce z Kabulu.Za kulisami buntu obyczajowego w Afganistanie', 'Jenny Nordberg'),
        Book('Czyje jest nasze życie?', 'Olga Drenda, Bartłomiej Dobroczyński'),
        Book('Detroit. Sekcja zwłok Ameryki', 'Charlie LeDuff'),
        Book('Głód', 'Martín Caparrós'),
        Book('Hajstry. Krajobraz bocznych dróg', 'Adam Robiński'),
        Book('Jak rozmawiać o książkach, których się nie czytało', 'Pierre Bayard'),
        Book('J jak jastrząb', 'Helen Macdonald'),
        Book('Łódź 370, Annah Björk',' Mattias Beijmo'),
        Book('Matka młodej matki', 'Justyna Dąbrowska'),
        Book('Mężczyźni objaśniają mi świat', 'Rebecca Solnit'),
        Book('Nie ma się czego bać', 'Justyna Dąbrowska'),
        Book('Obwód głowy', 'Włodzimierz Nowak'),
        Book('Ostatnie dziecko lasu', 'Richard Louv'),
        Book('Polska odwraca oczy', 'Justyna Kopińska'),
        Book('Powrócę jako piorun', 'Maciej Jarkowiec'),
        Book('Simona. Opowieść o niezwyczajnym życiu Simony Kossak', 'Anna Kamińska'),
        Book('Szlaki.Opowieści o wędrówkach', 'Robert Macfarlane'),
        Book('Wykluczeni', 'Artur Domosławski')
    ]
    anna=Human('anna', 'dzwon')
    anna.changeAge(22)
    jan=Human()
    jan.changeAge(69)
    [jan.addFavouriteBook(books[i]) for i in range(5, len(books)-1, 3)]
    jan.printData()

